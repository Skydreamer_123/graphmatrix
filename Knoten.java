public class Knoten 
{
    private String bezeichner;
    
    public Knoten(String bezeichner)
    {
        this.bezeichner = bezeichner;
    }
    
    public String bezeichnerGeben()
    {
        return this.bezeichner;
    }
    
    public String bezFormatGeben(int anzahl)
    {       
        String name = this.bezeichnerGeben();
        String addition = "___";
        
        String ll = name + addition;
            
        if(name.length() < 3)
        {
            return ll.substring(0,anzahl);
        }
        else
        {
            return name.substring(0,anzahl);
        }
    }
}