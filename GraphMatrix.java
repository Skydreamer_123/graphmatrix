public class GraphMatrix
{
    private int anzahlKnoten;
    private Knoten[] knoten; //Speichert Knoten 
    private int[][] matrix; //Tabelle
    private boolean[] besucht;
    private int[] distanz;
    private int[] kommtVon;

    public GraphMatrix(int groesse)
    {
        anzahlKnoten = 0;
        knoten= new Knoten[groesse]; //Speichert alle Knoten
        matrix = new int[groesse][groesse]; //2 demensionales Array
        besucht = new boolean[groesse]; //besucht
        distanz = new int[groesse]; 
        kommtVon = new int[groesse];

        for(int i1 = 0; i1 < knoten.length ; i1++) //Durchlauf ersten Reihe
        {
            for(int i2 = 0; i2 < knoten.length ; i2++ ) //Durchlauf 2. Reihe
            {
                matrix[i1][i2] = Integer.MIN_VALUE;
            }
        }

        for(int i1 = 0; i1 < knoten.length ; i1++)
        {
            matrix[i1][i1] = 0;
        }
    }

    public void knotenEinfuegen(String name)
    {
        if(this.anzahlKnoten < this.knoten.length)
        {
            if(this.knotenNummer(name) == -1)
            {
                knoten[this.anzahlKnoten] = new Knoten(name);
                anzahlKnoten = anzahlKnoten + 1;
            } 
            else 
            {
                System.out.println("Bereits vorhanden.");
            }
        } 
        else 
        {
            System.out.println("Matrix voll.");
        }
    }

    public void kantenEinfuegen(String von, String nach, int gewichtung)
    {
        int vonNummer = knotenNummer(von);
        int nachNummer = knotenNummer(nach);

        if(vonNummer != -1 && nachNummer != -1)
        {
            this.matrix[vonNummer][nachNummer] = gewichtung;
            //this.matrix[nachNummer][vonNummer] = gewichtung;
        }
    }

    public int knotenNummer(String name)
    {
        for(int i = 0; i < knoten.length; i++)
        {
            if(this.knoten[i] != null)
            {
                if(this.knoten[i].bezeichnerGeben() == name)
                {
                    return i;
                }
            }
        }
        return -1;
    }

    public void ausgeben()
    {
        System.out.println();
        System.out.println();
        System.out.print("      ");

        for(int i1 = 0; i1< knoten.length; i1++)
        {
            if(knoten[i1] != null)
            {
                System.out.print(knoten[i1].bezFormatGeben(3) + " ");
            }
            else
            {
                System.out.print("N/A ");
            }
        }

        for(int i2 = 0; i2< knoten.length; i2++)
        {
            System.out.println();

            if(knoten[i2] != null)
            {
                System.out.print("   " + knoten[i2].bezFormatGeben(3)+ "");
            }
            else
            {
                System.out.print("   " + "N/A");
            }

            for(int i3 = 0; i3< knoten.length; i3++)
            {
                int gewicht = matrix[i2][i3];
                if(matrix[i2][i3] == Integer.MIN_VALUE)
                {
                    System.out.print(" @  ");
                }
                else
                {
                    String stringZahl = String.valueOf(matrix[i2][i3]);
                    System.out.print(cut(3 , stringZahl) + " "); 
                }
            }
        }
    }

    public int knotenAnzahlGeben()
    {
        return anzahlKnoten;
    }

    public int kantenGewichtGeben(String von, String nach)
    {
        int vonNummer = knotenNummer(von);
        int nachNummer = knotenNummer(nach);

        return matrix[vonNummer][nachNummer];
    }

    public int kantenGewichtGebenINT(int i1, int i2)
    {
        return matrix[i1][i2];
    }

    public String cut(int anzahl, String n) // == bezFormatGeben
    {       
        String addition = "___";
        String ll = n + addition;

        if(n.length() < 3)
        {
            return ll.substring(0,anzahl);
        }
        else
        {
            return n.substring(0,anzahl);
        }
    }

    public void tiefensuche(String startKnoten)
    {
        int startNummer = knotenNummer(startKnoten);

        for(int i1 = 0; i1< knoten.length; i1++)
        {
            besucht[i1] = false;
        }

        besuchen(startNummer);
    }

    public void besuchen(int knotenNummer)
    {
        besucht[knotenNummer] = true;
        System.out.println(knoten[knotenNummer].bezeichnerGeben() + "; ");

        for(int i1 = 0; i1< knoten.length -1; i1++)
        {
            if((matrix[knotenNummer][i1] > Integer.MIN_VALUE) && (besucht[i1] == false))
            {
                besuchen(i1);
            }   
        }

        System.out.println(knoten[knotenNummer].bezeichnerGeben() + " abgeschlossen");
    }

    public int minKnoten()
    {
        int minPos = 0;
        int minWert = Integer.MAX_VALUE;

        for(int i = 0; i <= anzahlKnoten - 1; i++)
        {
            if((besucht[i] == false) && (minWert > distanz[i]))
            {
                minWert = distanz[i];
                minPos = i;
            }

        }
        return minPos;
    }

    public void kuerzesterWeg(String startKnoten, String zielKnoten)
    {
        int startNummer = knotenNummer(startKnoten);
        int zielNummer = knotenNummer(zielKnoten);

        int knotenNummer;
        int neueDistanz;

        for(int i = 0; i <= anzahlKnoten - 1; i++) //initialisieren von besucht ubd distanz
        {
            besucht[i] = false;
            distanz[i] = Integer.MAX_VALUE;
        }
        
        distanz[startNummer] = 0;
        kommtVon[startNummer] = startNummer;
        
        for(int i1 = 0; i1 <= anzahlKnoten - 1; i1++)
        {
            knotenNummer = minKnoten();
            besucht[knotenNummer] = true;
            
            for(int abzweigNummer = 0; abzweigNummer <= anzahlKnoten -1; abzweigNummer++) //abzweigNummer
            {
                if((matrix[knotenNummer][abzweigNummer] > 0) &&(besucht[abzweigNummer] == false))
                {
                    neueDistanz = distanz[knotenNummer] + matrix[knotenNummer][abzweigNummer]; //neue Distanz

                    if(neueDistanz < distanz[abzweigNummer])  //relaxationsschritt
                    {
                        distanz[abzweigNummer] = neueDistanz;
                        kommtVon[abzweigNummer] = knotenNummer;
                    }
                }
            }
        }
        
        System.out.println("Die kürzeste Distanz von " + startKnoten + " nach " + zielKnoten + " beträgt: " + distanz[zielNummer]);
        String pfad = zielKnoten;
        knotenNummer = zielNummer;
        
        
        while(knotenNummer != startNummer)
        {
            knotenNummer = kommtVon[knotenNummer];
            pfad = knoten[knotenNummer].bezeichnerGeben() + " / " + pfad;
        }
        
        System.out.println("der kürzeste Pfad von " + startKnoten + " nach " + zielKnoten + " ist: " + pfad);
    }
       
}