public class Testklasse
{
    GraphMatrix gm; //for testing
    
    public Testklasse()
    {
        gm = new GraphMatrix(3);
        
        gm.knotenEinfuegen("Augsburg");
        gm.knotenEinfuegen("Berlin");
        gm.knotenEinfuegen("Charls");
                
        gm.kantenEinfuegen("Augsburg", "Berlin", 10);
        gm.kantenEinfuegen("Berlin", "Charls", 111);
        
        gm.knotenNummer("Augsburg");
        gm.knotenNummer("Charles");
        
        gm.kuerzesterWeg("Augsburg", "Charls");
    }
}